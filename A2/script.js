var gameBoard;
var ORANGE = "ORANGE";
var RED = "RED";
var BLACK = "BLACK";
var level = 1;
var BLACK_SPEED_1 = 150;
var BLACK_SPEED_2 = 200;
var BLACK_SCORE = 5;
var RED_SPEED_1 = 75;
var RED_SPEED_2 = 100;
var RED_SCORE = 3;
var ORANGE_SPEED_1 = 60;
var ORANGE_SPEED_2 = 80;
var ORANGE_SCORE = 1;
var time = 0;
var monster_num = 0;
var foods = [];
var monsters = [];
var ctx;
var interval;
var is_pause = false;
var spawn = 0;
var spawn_count = 0;
var score = 0;
var shouldMove = true;

function play(){
	document.getElementById("menus").style.display = 'none';
    document.getElementById("game").style.display = 'block';
	document.getElementById("endGame").style.display = 'none';
    document.getElementById("gameBoard").style.display = 'block';
	init();
	interval = setInterval(main, 10);
}

function getLevel(radio_group){
	for(var i = 0; i < radio_group.length; i++){
		if(radio_group[i].checked){
			return radio_group[i].value;
		}
	}
}

function init(){
	gameBoard = document.getElementById("gameBoard");
	ctx = gameBoard.getContext("2d");
	monster_num = 0;
	foods = [];
	monsters = [];
	is_pause = false;
	spawn = 0;
	spawn_count = 0;
	score = 0;
	time = 0;
	count = 0;
	var q = location.search.substring(1);
	if(q.length > 0){
		level = parseInt(q.slice(-1));
	}
	for(var i = 0; i < 5; i++){
		foods[i] = new food(Math.floor((Math.random() * 360) + 20), Math.floor((Math.random() * 459) + 120));
	}
}


function food(x, y){
	var x;
	var y;

	this.x = x;
	this.y = y;

	this.drawFood = function(ctx){
		ctx.fillStyle = "#FF0000";
		ctx.strokeStyle = "#00FF00";

		ctx.beginPath();
		ctx.arc(this.x, this.y, 10, 0, 2*Math.PI);
		ctx.fill();
		ctx.closePath();

		ctx.fillStyle = "#00FF00";

		ctx.beginPath();
		ctx.moveTo(this.x, this.y - 5);
		ctx.ellipse(this.x + 5, this.y - 5, 3, 8, 45 * Math.PI/180, 0, 2 * Math.PI);
		ctx.fill();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(this.x, this.y - 5);
		ctx.bezierCurveTo(this.x, this.y - 10, this.x - 10, this.y - 10, this.x - 10, this.y - 5);
		ctx.stroke();
		ctx.closePath();
	};
}


function main(){
	gameBoard = document.getElementById("gameBoard");
	ctx = gameBoard.getContext("2d");
	
	if(count > spawn){
		type = Math.floor((Math.random() * 10) + 1);
		if(type > 7){
			monsters[monster_num] = new monster(Math.floor((Math.random() * 390) + 10), BLACK);
		}
		else if(type > 4){
			monsters[monster_num] = new monster(Math.floor((Math.random() * 390) + 10), RED);
		}
		else{
			monsters[monster_num] = new monster(Math.floor((Math.random() * 390) + 10), ORANGE);
		}
		monster_num ++;
		spawn = Math.random() * 2 + 1;
		//spawn = 1;
		count = 0;
	}
	ctx.clearRect(0, 0, 400, 600);
	monsters.sort(function(a, b){
		if(!a.alive){
			return 1;
		}
		else if(a.type == b.type){
			return b.x - a.x;
		}
		else if(a.type == BLACK){
			return -1;
		}
		else if(a.type == ORANGE){
			return 1;
		}
		else if(a.type == RED && b.type == BLACK){
			return 1;
		}
		else{
			return -1;
		}
	});
	for (m in monsters){
		if(monsters[m].alive){
			if(foods.length < 1){
				end(false);
				break;
			}
			monsters[m].move(m);
			monsters[m].eat();
			monsters[m].drawMonster(ctx);
		}
		else if(monsters[m].death_time < 0.95){
			monsters[m].fadeOut();
			monsters[m].drawMonster(ctx);
		}
	}
	for (f in foods){
		foods[f].drawFood(ctx);
	}
	


	time += 0.01;
	count += 0.01;
	document.getElementById("timer").innerHTML = (60 - time).toFixed(1) + ' Sec';
	document.getElementById("score").innerHTML = 'Score: ' + score.toString();
	if(time >= 60){
		end(true);
	}
}

function monster(x1, type1){
	var x, y, type, closest_index, distance, temp, score, speed, alive, angle, leg, death_time;
	x = x1;
	type = type1;
	this.type = type1;
	y = 0;
	this.alive = true;
	this.x = x1;
	this.y = 0;
	leg = 2;
	death_time = 0;
	this.death_time = 0;
	if(type == BLACK){
		this.score = BLACK_SCORE;
		if(level == 1){
			speed = BLACK_SPEED_1;
		}
		else{
			speed = BLACK_SPEED_2;
		}
	}		
	else if(type == RED){
		this.score = RED_SCORE;
		if(level == 1){
			speed = RED_SPEED_1;
		}
		else{
			speed = RED_SPEED_2;
		}
	}		
	else{
		this.score = ORANGE_SCORE;
		if(level == 1){
			speed = ORANGE_SPEED_1;
		}
		else{
			speed = ORANGE_SPEED_2;
		}

	}
	this.fadeOut = function(){
		death_time += 0.005;
		this.death_time += 0.005;
	};
	this.drawMonster = function(ctx){
		if(leg == 3){
			leg = 4;
		}
		else{
			leg = 3;
		}
		ctx.strokeStyle = "rgba(0, 0, 0, " + (1 - death_time).toString() + ")";
		if(type == BLACK){
			ctx.fillStyle = "rgba(0, 0, 0, " + (1 - death_time).toString() + ")";
		}
		else if(type == RED){
			ctx.fillStyle = "rgba(255, 0, 0, " + (1 - death_time).toString() + ")";
		}
		else{
			ctx.fillStyle = "rgba(255, 102, 0, " + (1 - death_time).toString() + ")";
		}
		ctx.beginPath();
		ctx.ellipse(x, y, 5, 20, 2 * Math.PI - angle, 0, 2 * Math.PI);
		ctx.fill();
		ctx.closePath();

		ctx.fillStyle = "rgba(0, 0, 0, " + (1 - death_time).toString() + ")";
		ctx.beginPath();
		ctx.arc(x + 20 * Math.sin(angle), y + 20 * Math.cos(angle), 5, 0, 2*Math.PI);
		ctx.fill();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x + 10 * Math.sin(angle), y + 10 * Math.cos(angle));
		ctx.lineTo(x + 10 * Math.sin(angle) + 10 * Math.sin(angle + Math.PI / leg), y + 10 * Math.cos(angle) + 10 * Math.cos(angle + Math.PI / leg));
		ctx.stroke();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x + 10 * Math.sin(angle), y + 10 * Math.cos(angle));
		ctx.lineTo(x + 10 * Math.sin(angle) + 10 * Math.sin(angle + Math.PI - (Math.PI / leg)),
					y + 10 * Math.cos(angle) + 10 * Math.cos(angle + Math.PI - (Math.PI / leg)));
		ctx.stroke();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x + 10 * Math.sin(angle), y + 10 * Math.cos(angle));
		ctx.lineTo(x + 10 * Math.sin(angle) + 10 * Math.sin(angle - Math.PI / leg),
					y + 10 * Math.cos(angle) + 10 * Math.cos(angle - Math.PI / leg));
		ctx.stroke();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x + 10 * Math.sin(angle), y + 10 * Math.cos(angle));
		ctx.lineTo(x + 10 * Math.sin(angle) + 10 * Math.sin(angle - Math.PI + (Math.PI / leg)),
					y + 10 * Math.cos(angle) + 10 * Math.cos(angle - Math.PI + (Math.PI / leg)));
		ctx.stroke();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x - 10 * Math.sin(angle), y - 10 * Math.cos(angle));
		ctx.lineTo(x - 10 * Math.sin(angle) + 10 * Math.sin(angle + Math.PI / leg), y - 10 * Math.cos(angle) + 10 * Math.cos(angle + Math.PI / leg));
		ctx.stroke();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x - 10 * Math.sin(angle), y - 10 * Math.cos(angle));
		ctx.lineTo(x - 10 * Math.sin(angle) + 10 * Math.sin(angle + Math.PI - (Math.PI / leg)),
					y - 10 * Math.cos(angle) + 10 * Math.cos(angle + Math.PI - (Math.PI / leg)));
		ctx.stroke();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x - 10 * Math.sin(angle), y - 10 * Math.cos(angle));
		ctx.lineTo(x - 10 * Math.sin(angle) + 10 * Math.sin(angle - Math.PI / leg),
					y - 10 * Math.cos(angle) + 10 * Math.cos(angle - Math.PI / leg));
		ctx.stroke();
		ctx.closePath();

		ctx.beginPath();
		ctx.moveTo(x - 10 * Math.sin(angle), y - 10 * Math.cos(angle));
		ctx.lineTo(x - 10 * Math.sin(angle) + 10 * Math.sin(angle - Math.PI + (Math.PI / leg)),
					y - 10 * Math.cos(angle) + 10 * Math.cos(angle - Math.PI + (Math.PI / leg)));
		ctx.stroke();
		ctx.closePath();
	};
	this.eat = function(){
		for(var i = 0; i < foods.length; i++){
			temp = Math.sqrt((foods[i].x - x) * (foods[i].x - x) + (foods[i].y - y) * (foods[i].y - y));
			if(temp < 35){
				foods.splice(i, 1);
			}
		}
	};
	this.move = function(m){
		distance = 10000000;
		for(var i = 0; i < foods.length; i++){
			temp = Math.sqrt((foods[i].x - x) * (foods[i].x - x) + (foods[i].y - y) * (foods[i].y - y));
			if(temp < distance){
				distance = temp;
				closest_index = i;
			}
		}
		
		var x_diff = foods[closest_index].x - x;
		var y_diff = foods[closest_index].y - y;
		angle = Math.atan2(x_diff, y_diff);
		var x_change = speed * Math.sin(angle) / 100;
		var y_change = speed * Math.cos(angle) / 100;
		shouldMove = true;

		for(n in monsters){
			if(monsters[n].alive){
				if(m > n){
					if(Math.abs(x + x_change * 20 - monsters[n].x) < 25 && Math.abs(y + y_change * 20 - monsters[n].y) < 25){
						shouldMove = false;
						break;
					}
				}
			}
		}
		if(shouldMove){
			x += x_change;
			y += y_change;
			this.x += x_change;
			this.y += y_change;
		}
		
	};
}

function pause(){
	if(!is_pause){
		clearInterval(interval);
		is_pause = !is_pause;
		document.getElementById("pause").innerHTML = 'Play';
	}
	else{
		interval = setInterval(main, 10);
		is_pause = !is_pause;
		document.getElementById("pause").innerHTML = 'Pause';
	}
	
}

function kill(event){
    var rect = gameBoard.getBoundingClientRect();
	var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    if(!is_pause){
    	for (m in monsters){
			if(monsters[m].alive){
				if(Math.sqrt((monsters[m].x - x) * (monsters[m].x - x) + (monsters[m].y - y) * (monsters[m].y - y)) < 30){
					monsters[m].alive = false;
					score += monsters[m].score;
					document.getElementById("score").innerHTML = 'Score: ' + score.toString();
				}
			}
		}
    }
    
}

function end(win){
	if(win && level == 1){
		setHighScore(score);
		level = 2;
		init();
	}
	else{
		setHighScore(score);
		clearInterval(interval);
		document.getElementById("endGame").style.display = 'block';
    	document.getElementById("gameBoard").style.display = 'none';
	}
}
function restart(){
	document.getElementById("endGame").style.display = 'none';
    document.getElementById("gameBoard").style.display = 'block';
    play();
}
function exit(){
	document.getElementById("menus").style.display = 'block';
    document.getElementById("game").style.display = 'none';
    document.getElementById("highScore").innerHTML = 'Highscore: ' + localStorage.getItem("highScore" + level.toString()).toString();
}

function setHighScore(highScore){
	if(localStorage.getItem("highScore" + level.toString()) < highScore){
		localStorage.setItem("highScore" + level.toString(), highScore.toString());
	}
}

function displayHighScore(){
	level = getLevel(document.getElementsByName("level"));
	document.getElementById("highScore").innerHTML = 'Highscore: ' + localStorage.getItem("highScore" + level.toString()).toString();
}