
http = require('http');
fs = require('fs');
path = require('path');
url = require("url");

PORT = 3000;
PATH_TO_JSON = 'favs.json'

MIME_TYPES = {
  '.html': 'text/html',
  '.css': 'text/css',
  '.js': 'text/javascript',
  '.txt': 'text/plain'
};
/*
/?info=all
/?info=friends
/?info=links
/?info=tweet
/?info=user
/?info=interest
*/

http.createServer(function(req, res){
  if (req.url == '/') {
    fs.readFile('index.html', function(err, data){
      res.writeHead(200, {
              'Content-Type': MIME_TYPES,
              'Content-Length': data.length
      });
      res.write(data);
      res.end();
    });
  }
  else if(req.url.substring(0,7) == '/?info='){
    fs.readFile(PATH_TO_JSON, function(err, data){
      if(err){
        throw err;
      }
      obj = JSON.parse(data);
      res.writeHead(200, {
              'Content-Type': 'text/html',
              'Content-Length': data.length
      });
      if(req.url == '/?info=all'){
        handleAll(obj, res);
        res.end();
      }
      else if(req.url == '/?info=friends'){
        handleFriends(obj, res);
        res.end();
      }
      else if(req.url == '/?info=links'){
        handleLinks(obj, res);
      }
      else if(req.url == '/?info=tweet'){
        handleAll(obj, res);
        res.end();
      }
      else if(req.url == '/?info=user'){
        handleAll(obj, res);
        res.end();
      }
      else if(req.url == '/?info=interest'){
        handleAll(obj, res);
        res.end();
      }
      
      
    });
  }
  else{
    res.end();
  }/*
    if(req.method.toLowerCase() == 'get'){
    }*/
}).listen(PORT);

console.log('Server running at http://127.0.0.1:' + PORT + '/');

handleAll = function(obj, res){
  for(var myKey in obj) {
    res.write(obj[myKey].user.screen_name);
    res.write(': ');
    res.write(obj[myKey].text);
    res.write('<br>');
    res.write('<br>');
  }
};
handleFriends = function(obj, res){
  for(var myKey in obj) {
    res.write(obj[myKey].user.id_str);
  }
};
handleLinks = function(obj, res){
  for(var myKey in obj) {
    if(obj[myKey].entities.urls.expended_url){
      res.write(obj[myKey].entities.urls.expended_url);
    }
  }

  res.end();
};
