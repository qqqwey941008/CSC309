var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var fs = require('fs');
var multer  = require('multer');
var upload = multer({ dest: 'public/image' });

/* GET home page. */
mongoose.connect('mongodb://localhost:27017/a4');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log('Connected to MongoDB');
});

var userSchema = mongoose.Schema({
  email: String,
  password: String,
  displayName: String,
  type: String,
  description: String,
  profilePicAdd: String
});

var Users = mongoose.model('users', userSchema);

var requestS = mongoose.Schema({
	request: String,
	count: Number
});

var Requests = mongoose.model('requests', requestS);

var loginS = mongoose.Schema({
	ip: String,
	device: String,
	location: String
});

var Logins = mongoose.model('logins', loginS);


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/register', function(req, res, next) {
  res.render('register', {error: ''});
});

router.get('/login', function(req, res, next) {
  res.render('login', {error: ''});
});

router.post('/login', function(req, res, next) {
	Users.findOne({email:req.body.email}, function(err, user){
		if(err){
			res.status(500).send(err);
			console.log(err);
			return;
		}
		if(user && user.password == req.body.password){
			req.session.user = user;
			updateLogin(req);
			res.redirect('/main/' + user._id);
		}
		else{
			res.render('login', {error: 'email password combination not valid'})
		}
	});
});

router.post('/register', function(req, res, next) {
	if(!req.body.email || !req.body.password || !req.body.cpassword || !req.body.username){
		res.render('register', { error: 'Please enter all required info' });
	}
	else if(req.body.password != req.body.cpassword){
		res.render('register', { error: 'Password does not match' });
	}
	else{
		Users.find(function(err, users) {
	    if (err) {
	      res.status(500).send(err);
	      console.log(err);
	      return;
	    }

	    // Send the books back to the user.
	   	if (users.length == 0){
	   		newUser = new Users({
			  	email: req.body.email,
			  	displayName: req.body.username,
			  	password: req.body.password,
			  	type: 'SAU',
			  	profilePicAdd: '/image/default.png',
			  	description: ''
				});
	   	}
	   	else{
	   		newUser = new Users({
			  	email: req.body.email,
			  	displayName: req.body.username,
			  	password: req.body.password,
			  	type: 'U',
			  	profilePicAdd: '/image/default.png',
			  	description: ''
				});
	   	}
	  });

		Users.findOne({email:req.body.email}, function(err, user){
			if(!user){
				newUser.save(function(err){
					if(err){
						res.status(500).send(err);
						console.log(err);
						return
					}
				});
				res.render('success'); 
			}
			else{
				res.render('register', { error: 'Email already exist' });
			}
		});
	}
});	

router.get('/main/:id', function(req, res, next){
	if(req.session.user && req.session.user._id == req.params.id){
		updateRequest('main');
		Users.find(function(err, users){
			allUsers = users;
		});
		Logins.find(function(err, l){
			logins = l;
		});
		Requests.find(function(err, r){
			requests = r;
		});
		Users.findById(req.params.id, function(err, user) {
	    if (err) {
	      res.status(500).send(err);
	      console.log(err);
	      return;
	    }
	    if(req.session.user.type != "U"){
	    	res.render('main', {users: allUsers, user: user, login:req.session.user, logins:logins, requests:requests});
	    }
	    else{
	    	res.render('main', {users: allUsers, user: user, login:req.session.user});
	    }
	  });
	}
	else{
		res.render('deny');
	}
});


router.get('/profile/:id', function(req, res, next){
	if(!req.session.user){
		res.render('deny');
	}
	else{
		updateRequest('profile');
		Users.findById(req.params.id, function(err, user) {
	    if (err) {
	      res.status(500).send(err);
	      console.log(err);
	      return;
	    }
	    if(req.session.user._id == req.params.id){
	    	res.render('profile', {user: user, type: "U", this: true, login:req.session.user});
	    }
	    else if(req.session.user.type == "SAU"){
	    	res.render('profile', {user: user, type: "SAU", this: true, login:req.session.user});
	    }
	    else if(req.session.user.type == "AU" && user.type == "U"){
	    	res.render('profile', {user: user, type: "AU", this: true, login:req.session.user});
	    }
	    else{
	    	res.render('profile', {user: user, type: "U", this: false, login:req.session.user});
	    }
	  });
	}
});

router.get('/edit/:id', function(req, res, next){
	if(!req.session.user){
		res.render('deny');
	}
	else{
		updateRequest('edit');
		Users.findById(req.params.id, function(err, user) {
	    if (err) {
	      res.status(500).send(err);
	      console.log(err);
	      return;
	    }
	    if(req.params.id == req.session.user._id){
	    	var cur = true;
	    }
	    else{
	    	var cur = false;
	    }
	    res.render('edit', {user: user, login:req.session.user, this:cur});
	  });
	}
});

router.post('/edit/:id', upload.single('displayImage'), function(req, res, next){
	if(!req.session.user){
		res.render('deny');
	}
	else{
		Users.findById(req.params.id, function(err, user) {
	    if (err) {
	      res.status(500).send(err);
	      console.log(err);
	      return;
	    }
	    if(req.params.id == req.session.user._id){
	    	var cur = true;
	    }
	    else{
	    	var cur = false;
	    }
			if(!req.body.opassword){
		    user.displayName = req.body.username;
		    user.description = req.body.description;
		    console.log(req.file);
		    if(req.file){
					user.profilePicAdd = '/image/' + req.file.filename;
		    }
		    user.save(function(){
		    	if(user._id == req.session.user._id){
			    	req.session.user = user;
		    		res.render('edit', {user: user, this: this, this:cur, login:req.session.user});
			    }
			    else{
						res.render('edit', {user: user, this: this, this:cur, login:req.session.user});
			    }
		    });
		  }
		  else if(user.password == req.body.opassword && req.body.cpassword == req.body.npassword){
		  	user.password = req.body.cpassword;
		  	user.save();
		    res.render('edit', {user: user, error: 'success', this: cur, login:req.session.user});
		  }
		  else{
		    res.render('edit', {user: user, error: 'Input is not valid', this: cur, login:req.session.user});
		  }
	  });
	}
});

router.get('/delete/:id', function(req, res, next){
	if(req.session.user && (req.session.user.type == "SAU" || req.session.user.type == "AU")){
		updateRequest('delete');
		Users.remove({_id: req.params.id}, function(err) {
	    if (err) {
	      res.status(500).send(err);
	      console.log(err);
	      return;
	    }
			res.redirect('/main/' + req.session.user._id); 
	  });
	}
	else{
		res.render('deny');
	}
});

router.get('/admin/:id', function(req, res, next){
	updateRequest('admin');
	if(req.session.user && req.session.user.type == "SAU"){
		Users.findById(req.params.id, function(err, user) {
	    if (err) {
	      res.status(500).send(err);
	      console.log(err);
	      return;
	    }
	    if(user.type == "AU"){
	    	user.type = "U";
	    }
	  	else{
	  		user.type = "AU";
	  	}
	  	user.save();
			res.redirect('back');
	  });
	}
	else{
		res.render('deny');
	}
});

router.get('/re', function(req, res, next){
	if(req.session.user){
		res.redirect('/main/' + req.session.user._id);
	}
	else{
		res.redirect('/');
	}
});

module.exports = router;


updateLogin = function(req){
	var ip = req.headers['x-forwarded-for'] || 
     req.connection.remoteAddress || 
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;
  console.log(ip);
	console.log(req.headers['user-agent']);
	login = new Logins({
		ip: ip,
		device: req.headers['user-agent'],
		location: 'N/A'
	});
	login.save(function(err){
		if(err){
			res.status(500).send(err);
			console.log(err);
			return
		}
	});
}

updateRequest = function(url){
	Requests.findOne({request: url}, function(err, request){
		if(err){
			res.status(500).send(err);
			console.log(err);
			return;
		}
		if(!request){
			newRequest = new Requests({
				request: url,
				count: 1
			});
			newRequest.save(function(err){
				if(err){
					res.status(500).send(err);
					console.log(err);
					return
				}
			});
		}
		else{
			request.count += 1;
			request.save();
		}
	});
}